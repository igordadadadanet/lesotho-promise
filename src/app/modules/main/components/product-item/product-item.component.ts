import {Component, Input} from '@angular/core';
import {MainGoodsCard} from "../../../../models/interfaces/interfaces";


@Component({
    selector: 'app-product-item',
    templateUrl: './product-item.component.html',
    styleUrls: ['../products/products.component.scss']
})
export class ProductItemComponent {
    @Input('card') card: MainGoodsCard = {
        title: '',
        imgSrc: '',
        link: '',
        type: ''
    }

}
