import {Component, OnInit} from '@angular/core';
import {DataType, isType, MainGoodsCard} from "../../../../models/interfaces/interfaces";
import {StateService} from "../../../../services/state.service";


@Component({
    selector: 'app-products',
    templateUrl: './products.component.html',
    styleUrls: ['./products.component.scss'],
})
export class ProductsComponent implements OnInit {
    private _url = 'assets/jsons/main-cloth-class.json';

    public loading = false;

    public mainGoodsCardsArray: MainGoodsCard[] = [];

    constructor(private state: StateService) {
    }

    ngOnInit(): void {
        this.loading = true;
        this.state.getData(this._url)
            .subscribe((data: DataType[]) => {
                this.loading = false;
                if (isType<MainGoodsCard>(data, 'type')) this.mainGoodsCardsArray = data;
            })
    }

}
