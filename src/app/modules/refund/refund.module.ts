import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RefundRoutingModule} from './refund-routing.module';
import {ReturnComponent} from "./components/return/return.component";


@NgModule({
    declarations: [
        ReturnComponent
    ],
    imports: [
        CommonModule,
        RefundRoutingModule
    ]
})
export class RefundModule {
}
