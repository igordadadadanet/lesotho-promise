import {Component, EventEmitter, Inject, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {IProduct} from "../../../../models/interfaces/interfaces";
import {StateService} from "../../../../services/state.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ajax, AjaxError, AjaxResponse} from "rxjs/ajax";
import {asyncScheduler, Subscription, switchMap, takeUntil} from "rxjs";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {OrderModalComponent} from "../../../../components/modals/order-modal/order-modal.component";
import {_appUrl, _serverUrl} from "../../../../common/url";
import {AssortmentRestService} from "../../services/assortment-rest.service";
import {SERVER_URL} from "../../../../app.module";


@Component({
    selector: 'app-merchandise',
    templateUrl: './merchandise.component.html',
    styleUrls: ['./merchandise.component.scss']
})
export class MerchandiseComponent implements OnInit, OnDestroy {
    private currentId: string;
    public isNotAvailableSubmit: boolean = false;
    modalRef?: BsModalRef;
    private routerParamSubscription!: Subscription;
    public userForm: FormGroup = new FormGroup({//todo ограничить длину, поиск на script и т.д.
        'userName': new FormControl('', [Validators.required, Validators.maxLength(30)]),
        'userPhone': new FormControl('', [Validators.required, Validators.pattern('[0-9]{10}')]),
        'userComments': new FormControl('', [Validators.maxLength(100)])
    })
    public currentMerch: IProduct;

    //----------------

    private destroySub = new EventEmitter<void>();

    constructor(public route: ActivatedRoute,
                private state: StateService,
                public router: Router, private modalService: BsModalService,
                private assortmentRestService: AssortmentRestService,
                @Inject(SERVER_URL) private serverUrl: string) {
    }

    ngOnInit(): void {
        this.route.paramMap
            .pipe(
                takeUntil(this.destroySub),
                switchMap((data: ParamMap) => {
                    const currentId = data.get('id');
                    return this.assortmentRestService.getProductById(currentId);
                })
            ).subscribe((data: IProduct) => {
                this.currentMerch = data;
                console.warn(this.currentMerch)

            }
        )
    }

    submit() {
        this.isNotAvailableSubmit = true;
        this.modalRef = this.modalService.show(OrderModalComponent);
        this.modalRef.content.status = 'loading';
        const url = `${this.serverUrl}/telegram`
        const telegramBot = ajax({
            url,
            method: 'POST',
            body: {
                ...this.userForm.value,
                merch: `${_appUrl}${this.router.url}`
            }
        })

        telegramBot.pipe(
            takeUntil(this.destroySub)
        ).subscribe((response: AjaxResponse<any>) => {
                this.userForm.reset();
                this.blockSubmitButton();
                this.modalRef!.content.status = 'complete';
            },
            (reject: AjaxError) => {
                this.blockSubmitButton();
                this.modalRef!.content.status = 'error';
            },
        )
    }

    private blockSubmitButton() {
        asyncScheduler.schedule(() => {
            this.isNotAvailableSubmit = false;
        }, 4000)
    }

    // openModal() {
    //     this.modalRef = this.modalService.show(AuthModalComponent);
    // }

    ngOnDestroy() {
        this.destroySub.next();
        this.destroySub.complete();
    }
}