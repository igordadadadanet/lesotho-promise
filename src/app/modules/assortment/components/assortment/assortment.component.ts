import {Component, EventEmitter, OnDestroy, OnInit} from '@angular/core';
import {IProduct} from "../../../../models/interfaces/interfaces";
import {RouterOutlet} from "@angular/router";
import {StateService} from "../../../../services/state.service";
import {wrapperTransition} from "../../../../animations/merchandise.animations";
import {PaginationService} from "../../services/pagination.service";
import {QueryParamsService} from "../../../../services/query-params.service";
import {AssortmentRestService} from "../../services/assortment-rest.service";
import {take, takeUntil} from "rxjs";
import {AdminRestService} from "../../../admin/services/admin-rest.service";


@Component({
    selector: 'app-assortment',
    templateUrl: './assortment.component.html',
    styleUrls: ['./assortment.component.scss'],
    animations: [wrapperTransition]
})
export class AssortmentComponent implements OnInit, OnDestroy {
    private destroySub = new EventEmitter<void>();
    public assortmentArray: IProduct[] = [];
    public loading = false;

    constructor(private state: StateService,
                private queryParamsService: QueryParamsService,
                private paginationService: PaginationService,
                private assortmentRestService: AssortmentRestService,
                private adminRestService: AdminRestService) {
    }

    ngOnInit(): void {
        this.getAssortments();
        this.adminRestService.deleteProductEvent
            .pipe(takeUntil(this.destroySub))
            .subscribe(() => {
                this.getAssortments();
            })
        // this.queryParamsService.activatedRoute.queryParams
        //     .pipe(takeUntil(this.destroySub))
        //     .subscribe(() => {
        //         this.loading = true;
        //         this.state.getData(this._url, true)
        //             .subscribe((data: DataType[]) => {
        //
        //                 if (isType<IProduct>(data, 'type')) {
        //
        //                     this.loading = false;
        //                     this.assortmentArray = this.paginationService.rendererPageData(data);
        //                     //каждый раз при рендере продукт листа
        //                 }
        //             });
        //     });
    }

    private getAssortments(): void {
        this.assortmentRestService.getAssortment()
            .pipe(take(1))
            .subscribe((products: IProduct[]) => {
                this.assortmentArray = products;
            })
    }

    ngOnDestroy() {
        this.destroySub.next();
        this.destroySub.complete();
    }

    prepareRoute(outlet: RouterOutlet) {
        return outlet.activatedRouteData['animation'] ? 'start' : 'end';
    }

}
