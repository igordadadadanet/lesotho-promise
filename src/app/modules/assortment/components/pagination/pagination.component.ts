import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { QueryParamsService } from "../../../../services/query-params.service";
import { Params } from "@angular/router";
import { PaginationService } from "../../services/pagination.service";
import { filter, Subscription } from "rxjs";

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements AfterViewInit, OnDestroy, OnInit {
  public pageCount: any[] = [1]

  public currentPage: number = 1;

  public itemsPerPage: number = 9;

  @ViewChild('pagination') pagination!: ElementRef;

  private queryParamsSubscription!: Subscription;

  private pageCountSubscription!: Subscription;

  constructor(private queryParamsService: QueryParamsService, private paginationService: PaginationService) {

  }

  ngAfterViewInit() {

  }

  ngOnInit() {
    this.pageCount.length = this.paginationService.pageCount;
    //подписываемся на изменения страниц для пагинации в сервисе пагинации
    this.pageCountSubscription = this.paginationService.pageCountStream$.subscribe((pageCount: number) => {
      this.pageCount.length = pageCount;
    })
    this.queryParamsSubscription = this.queryParamsService.activatedRoute.queryParams.pipe(
        //проблема квери параметров здесь!!!!!
        filter((param: any) => {
          return param;
        })
    ).subscribe((params: Params) => {

          if (params['page']) {
            this.currentPage = +params['page'] > this.pageCount.length ? this.pageCount.length : +params['page'];
          }
          if (params['itemsPerPage']) this.itemsPerPage = +params['itemsPerPage'];
          //почему не пушатся оба параметра?
          this.paginationService.setRendererPageParams('itemsPerPage', this.itemsPerPage);
          this.paginationService.setRendererPageParams('page', this.currentPage);

        }
    )


  }

  ngOnDestroy() {
    //отписываемя от подписки на изменение pageCount
    this.pageCountSubscription.unsubscribe();
    this.queryParamsSubscription.unsubscribe();
  }

  togglePageLink(event: Event) {
    const navElement: HTMLElement = event.target as HTMLElement;

    if (navElement.classList.contains('page-item') || navElement.classList.contains('disabled')) return

    const newActiveLink = navElement.innerHTML;

    switch (newActiveLink) {
      case ('Назад') :
        this.currentPage--;

        break;
      case ('Вперед') :
        this.currentPage++;

        break;
      default :
        this.currentPage = +newActiveLink;

    }

    //при переключении страницы, передаем значение из компонента в сервис и в строку параметров
    this.paginationService.setRendererPageParams('page', this.currentPage);
  }
}
