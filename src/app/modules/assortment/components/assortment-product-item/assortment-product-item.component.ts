import {Component, EventEmitter, Inject, Input, Output} from '@angular/core';
import {IMongoResultingMessage, IProduct} from "../../../../models/interfaces/interfaces";
import { WindowNavigateService } from "../../../../services/window-navigate.service";
import {AdminRestService} from "../../../admin/services/admin-rest.service";
import {take} from "rxjs";
import {ConfirmationService, MessageService} from "primeng/api";
import {SERVER_URL} from "../../../../app.module";

@Component({
    selector: 'app-assortment-product-item',
    templateUrl: './assortment-product-item.component.html',
    styleUrls: ['./assortment-product-item.component.scss']
})
export class AssortmentProductItemComponent {
    constructor(private windowNavService: WindowNavigateService,
                private adminRestService: AdminRestService,
                private confirmationService: ConfirmationService,
                private messageService: MessageService,
                @Inject(SERVER_URL) public serverUrl: string ) {
    }

    @Input('item') item: IProduct;

    scrollTo() {
        this.windowNavService.scrollTo()
    }

    public deleteItem(): void {
        this.confirmationService.confirm({
            message: 'Вы дейстивтельно хотите удалить этот товар?',
            header: 'Подтверждение удаления',
            icon: 'pi pi-info-circle',
            accept: () => {
                this.adminRestService.deleteProduct(this.item._id)
                    .pipe(take(1))
                    .subscribe((response: IMongoResultingMessage) => {
                        this.messageService.add({severity: 'success', summary: `Запись успешно удалена`, life: 1500});
                        this.adminRestService.deleteProductEvent.next();
                    })
            },
            reject: () => {
            }
        });
    }

}
