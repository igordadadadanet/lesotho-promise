import { Component, Input } from '@angular/core';
import { IProduct } from "../../../../models/interfaces/interfaces";


@Component({
    selector: 'app-assortment-products-list',
    templateUrl: './assortment-products-list.component.html',
    styleUrls: ['./assortment-products-list.component.scss']
})
export class AssortmentProductsListComponent {
    @Input('assortmentArray') assortmentArray: IProduct[] = [];

}