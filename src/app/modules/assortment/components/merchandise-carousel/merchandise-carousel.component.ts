import {Component, Inject, Input} from '@angular/core';
import { IProduct } from "../../../../models/interfaces/interfaces";
import {SERVER_URL} from "../../../../app.module";


@Component({
  selector: 'app-merchandise-carousel',
  templateUrl: './merchandise-carousel.component.html',
  styleUrls: ['./merchandise-carousel.component.scss']
})
export class MerchandiseCarouselComponent  {
  @Input('currentMerch') currentMerch!: IProduct;

  constructor(@Inject(SERVER_URL) public serverUrl: string) {
  }

}
