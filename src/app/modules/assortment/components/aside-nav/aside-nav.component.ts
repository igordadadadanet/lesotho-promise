import { Component, OnDestroy, OnInit } from '@angular/core';
import { FilterParams } from "../../../../models/interfaces/interfaces";
import { StateService } from "../../../../services/state.service";
import { QueryParamsService } from "../../../../services/query-params.service";
import { formCheckAnimations } from "../../../../animations/aside-nav-form-check.animations";
import { Subscription, tap } from "rxjs";


@Component({
    selector: 'app-aside-nav',
    templateUrl: './aside-nav.component.html',
    styleUrls: ['./aside-nav.component.scss'],
    animations: [formCheckAnimations]
})
export class AsideNavComponent implements OnInit, OnDestroy {
    public brand: any[];
    public type: any[];
    public gender: any[];
    public sizes: any[];

    constructor(private state: StateService, private queryParamsService: QueryParamsService) {
        this.brand = [];
        this.type = [];
        this.gender = [];
        this.sizes = [];
    }

    ngOnInit(): void {
        //подписываемся на изменения параметров в строке, записываем их в activeItems
        // this.routeSubscription = this.queryParamsService.activatedRoute.queryParams
        //     .subscribe((params: FilterParams) => {
        //         this.state.getFilterParams('brand').subscribe(data => {
        //             this.brand = this.state.auxiliary.prepareAsideParams(data).sort();
        //         })
        //
        //         this.state.getFilterParams('type').subscribe(data => {
        //             this.type = this.state.auxiliary.prepareAsideParams(data).sort();
        //         })
        //
        //         this.state.getFilterParams('gender').subscribe(data => {
        //             this.gender = this.state.auxiliary.prepareAsideParams(data).sort();
        //         })
        //
        //         this.state.getFilterParams('size').subscribe(data => {
        //             this.sizes = this.state.auxiliary.prepareAsideParams(data).sort();
        //         })
        //     })
    }

    //галочки на input которые указаны в query params
    isChecked(param: string, typeOfParam: string): boolean {
        return this.state.auxiliary.toArray(this.queryParamsService.queryParams[typeOfParam]).includes(param);
    }

    //удаляем и добавляем url query params
    toggleCheckBox(param: string, typeOfParam: string) {
        this.isChecked(param, typeOfParam) ?
            this.queryParamsService.deleteQueryParams(typeOfParam, param) :
            this.queryParamsService.addQueryParams(typeOfParam, param)
    }

    ngOnDestroy() {
    }
}