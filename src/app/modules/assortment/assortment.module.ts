import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AssortmentRoutingModule} from './assortment-routing.module';
import {AsideNavComponent} from "./components/aside-nav/aside-nav.component";
import {AssortmentComponent} from "./components/assortment/assortment.component";
import {AssortmentProductItemComponent} from "./components/assortment-product-item/assortment-product-item.component";
import {
    AssortmentProductsListComponent
} from "./components/assortment-products-list/assortment-products-list.component";
import {MerchandiseComponent} from "./components/merchandise/merchandise.component";
import {MerchandiseCarouselComponent} from "./components/merchandise-carousel/merchandise-carousel.component";
import {AppCommonModule} from "../../common/app-common.module";
import {PaginationComponent} from "./components/pagination/pagination.component";
import {ReactiveFormsModule} from "@angular/forms";
import {QueryParamsService} from "../../services/query-params.service";
import {PaginationService} from "./services/pagination.service";
import {AssortmentRestService} from "./services/assortment-rest.service";


@NgModule({
    declarations: [
        AsideNavComponent,
        AssortmentComponent,
        AssortmentProductItemComponent,
        AssortmentProductsListComponent,
        MerchandiseComponent,
        MerchandiseCarouselComponent,
        PaginationComponent
    ],
    imports: [
        CommonModule,
        AssortmentRoutingModule,
        AppCommonModule,
        ReactiveFormsModule
    ],
    providers: [
        QueryParamsService,
        PaginationService,
        AssortmentRestService
    ]
})
export class AssortmentModule {
}
