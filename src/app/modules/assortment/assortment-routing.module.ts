import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AssortmentComponent} from "./components/assortment/assortment.component";
import {MerchandiseComponent} from "./components/merchandise/merchandise.component";

// const assortmentRoutes: Routes = [
//     {path: 'merchandise/:id', component: MerchandiseComponent, data: {animation: true}}
// ];

const routes: Routes = [
    {
        path: '',
        component: AssortmentComponent,
        children: [
            {
                path: 'merchandise/:id',
                component: MerchandiseComponent,
                data: {animation: true}
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AssortmentRoutingModule {
}
