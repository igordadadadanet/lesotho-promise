import {Inject, Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {IProduct} from "../../../models/interfaces/interfaces";
import {SERVER_URL} from "../../../app.module";

@Injectable()
export class AssortmentRestService {
  constructor(private http: HttpClient, @Inject(SERVER_URL) private serverUrl: string) {
  }

  getAssortment(): Observable<IProduct[]> {
    return this.http.get<IProduct[]>(`${this.serverUrl}/products`);
  }

  getProductById(id: string | null): Observable<IProduct> {
    return this.http.get<IProduct>(`${this.serverUrl}/products/${id}`);
  }
}
