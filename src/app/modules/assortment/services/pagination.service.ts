import { Injectable } from '@angular/core';
import { IProduct } from "../../../models/interfaces/interfaces";
import { QueryParamsService } from "../../../services/query-params.service";
import { Subject } from "rxjs";


@Injectable()
export class PaginationService {
  private itemsPerPage: number = 9;

  private currentPage: number = 1;

  public pageCount = 1;

  public pageCountStream$ = new Subject<number>();

  private pageItems: IProduct[] = [];

  constructor(private queryParamsService: QueryParamsService) {
  }

  rendererPageData(pageItems: IProduct[]) {
    this.pageItems = pageItems;

    const startIndex = this.itemsPerPage * (this.currentPage - 1);

    const endIndex = this.itemsPerPage * this.currentPage;

    const resultPageList = this.pageItems.slice(startIndex, endIndex);

    this.pageCount = Math.ceil(pageItems.length / this.itemsPerPage)

    if(this.currentPage > this.pageCount) {
      this.currentPage = this.pageCount;
      this.setRendererPageParams('page', this.currentPage);
    }
    //запушили значение кол-ва листов в переменную
    this.pageCountStream$.next(this.pageCount);

    return resultPageList;
  }

  setRendererPageParams(typeOfParams: 'page' | 'itemsPerPage' ,value: number) {
    typeOfParams === 'page' ? this.currentPage = value : this.itemsPerPage = value;
    //принимаем активную страницу, устанавливаем ее в переменную сервиса

    //пушим значение в строковые параметры
    this.queryParamsService.addQueryParams(typeOfParams, value);

  }



}
