import {Component, OnInit} from '@angular/core';
import {Contacts, DataType, isType, Socials} from "../../../../models/interfaces/interfaces";
import {StateService} from "../../../../services/state.service";


@Component({
    selector: 'app-order',
    templateUrl: './order.component.html',
    styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {
    constructor(private state: StateService) {
    }

    private _socialUrl = 'assets/jsons/social.json';
    private _contactUrl = 'assets/jsons/contacts.json';

    contacts: Contacts = {
        phone: 'loading...',
        email: 'loading...',
        location: {
            address: 'loading...',
            position: 'loading...',
            iframePosition: ''
        }
    }

    socialMedia: Socials[] = [];

    ngOnInit(): void {
        this.state.getData(this._contactUrl)
            .subscribe((contacts: DataType[]) => {
                if (isType<Contacts>(contacts, 'email')) this.contacts = contacts[0];
            })
        this.state.getData(this._socialUrl)
            .subscribe((social: DataType[]) => {
                if (isType<Socials>(social, 'name')) this.socialMedia = social;
            })
    }

}
