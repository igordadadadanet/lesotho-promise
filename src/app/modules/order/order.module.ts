import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {OrderRoutingModule} from './order-routing.module';
import {OrderComponent} from "./components/order/order.component";
import {AppCommonModule} from "../../common/app-common.module";


@NgModule({
    declarations: [
        OrderComponent
    ],
    imports: [
        CommonModule,
        OrderRoutingModule,
        AppCommonModule
    ]
})
export class OrderModule {
}
