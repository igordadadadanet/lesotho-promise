import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { _serverUrl } from "../../../common/url";
import { ajax } from "rxjs/ajax";
import { map } from "rxjs/operators";
import { IProduct } from "../../../models/interfaces/interfaces";


interface User {
  id: string,
  token: string,
  roles: string[],
  user: string
}


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public user!: User;
  public isAuth: boolean = false;
  private url = `${_serverUrl}/auth/login`;

  constructor(private http: HttpClient) { }

  async login(email: string, password: string) {
    const response = await ajax({
      url: this.url, 
      method: 'POST',
      body: {
        email,
        password
      }
    })
        .pipe(
        map(data => data.response))
        .subscribe(response => {
          this.user = response as User;
          console.log(this.user);
          this.isAuth = true;
          return this.user.user;
        })
  }
}
