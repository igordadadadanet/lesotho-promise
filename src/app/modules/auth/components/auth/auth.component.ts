import { Component, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { takeUntil } from "rxjs";
import { map } from "rxjs/operators";
import { AuthService } from "../../services/auth.service";

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit, OnDestroy {
  public authForm: FormGroup = new FormGroup({
    'email': new FormControl('', [Validators.required, Validators.email]),
    'password': new FormControl('', [Validators.required, Validators.minLength(5)]),
  })

  constructor(private authService: AuthService) {
  }

  ngOnInit(): void {
  }

  async submit() {
    this.authService.login(this.authForm.value.email, this.authForm.value.password);
  }

  ngOnDestroy() {
  }

}
