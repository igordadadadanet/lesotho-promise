import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuthComponent} from "./components/auth/auth.component";
import {AuthService} from "./services/auth.service";
import {ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {AuthRoutingModule} from "./auth-routing.module";


@NgModule({
    declarations: [
        AuthComponent
    ],
    imports: [
        AuthRoutingModule,
        CommonModule,
        ReactiveFormsModule,
        HttpClientModule
    ],
    exports: [
        AuthComponent
    ],
    providers: [
        AuthService
    ]
})
export class AuthModule {
}
