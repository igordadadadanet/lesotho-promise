import {EventEmitter, Inject, Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {IMongoResultingMessage, IProduct, ISizes, ITypeOfClothing} from "../../../models/interfaces/interfaces";
import {Observable} from "rxjs";
import {SERVER_URL} from "../../../app.module";


@Injectable()
export class AdminRestService {
    public deleteProductEvent = new EventEmitter<void>();
    public clothingTypeChangeEvent = new EventEmitter<void>();
    public sizeChangeEvent = new EventEmitter<void>();
    constructor(private http: HttpClient, @Inject(SERVER_URL) private serverUrl: string) {
    }

    sendNewMerch(merch: IProduct): Observable<IProduct> {
      return this.http.post<IProduct>(`${this.serverUrl}/products`, merch);
    }

    sendNewTypeOfClothing(type: {name: string}) {
        return this.http.post<ITypeOfClothing>(`${this.serverUrl}/clothing-type`, type);
    }

    sendNewSize(size: {name: string}): Observable<ISizes> {
        return this.http.post<ISizes>(`${this.serverUrl}/sizes`, size);
    }

    getAllClothingTypes(): Observable<ITypeOfClothing[]> {
        return this.http.get<ITypeOfClothing[]>(`${this.serverUrl}/clothing-type`);
    }
    
    getAllSizes(): Observable<ISizes[]> {
        return this.http.get<ISizes[]>(`${this.serverUrl}/sizes`);
    }

    deleteSize(id: string): Observable<any> {
        return this.http.delete(`${this.serverUrl}/sizes/${id}`);
    }

    editSize(size: ISizes): Observable<ISizes> {
        return this.http.put<ISizes>(`${this.serverUrl}/sizes`, size);
    }

    deleteType(id: string): Observable<any> {
        return this.http.delete(`${this.serverUrl}/clothing-type/${id}`);
    }

    deleteProduct(id: string): Observable<IMongoResultingMessage> {
        return this.http.delete<IMongoResultingMessage>(`${this.serverUrl}/products/${id}`);
    }
}
