import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthService} from "../../../auth/services/auth.service";


@Injectable()
export class IsAdminGuard implements CanActivate {
    constructor(private authService: AuthService, private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        // !this.authService.isAuth && this.router.navigate(['auth']);
        // return this.authService.isAuth;
        return true;
    }

}
