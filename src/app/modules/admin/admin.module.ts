import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminRoutingModule} from "./admin-routing.module";
import {AdminComponent} from "./components/admin-component/admin.component";
import {CreateNewProductComponent} from "./components/create-new-product/create-new-product.component";
import {AppCommonModule} from "../../common/app-common.module";
import {ReactiveFormsModule} from "@angular/forms";
import {IsAdminGuard} from "./services/guards/is-admin.guard";
import {ImgDownloaderComponent} from './components/img-downloader/img-downloader.component';
import {CreateSizeComponent} from './components/create-size/create-size.component';
import {CreateClothingTypeComponent} from './components/create-clothing-type/create-clothing-type.component';
import {TabViewModule} from "primeng/tabview";
import {InputTextModule} from "primeng/inputtext";
import {DropdownModule} from "primeng/dropdown";


@NgModule({
    declarations: [
        AdminComponent,
        CreateNewProductComponent,
        ImgDownloaderComponent,
        CreateSizeComponent,
        CreateClothingTypeComponent
    ],
    imports: [
        CommonModule,
        AdminRoutingModule,
        AppCommonModule,
        ReactiveFormsModule,
        TabViewModule,
        InputTextModule,
        DropdownModule,
    ],
    exports: [
        AdminComponent
    ],
    providers: [
        IsAdminGuard
    ]
})
export class AdminModule {
}
