import {RouterModule, Routes} from "@angular/router";
import {AdminComponent} from "./components/admin-component/admin.component";
import {CreateNewProductComponent} from "./components/create-new-product/create-new-product.component";
import {NgModule} from "@angular/core";
import {IsAdminGuard} from "./services/guards/is-admin.guard";


const adminRoutes: Routes = [
    {
        path: '', component: AdminComponent,
        canActivate: [IsAdminGuard],
    }
];

@NgModule({
    imports: [RouterModule.forChild(adminRoutes)],
    exports: [RouterModule]
})
export class AdminRoutingModule {
}