import {Component, EventEmitter, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AdminRestService} from "../../services/admin-rest.service";
import {HttpErrorResponse} from "@angular/common/http";
import {IProduct, ISizes, ITypeOfClothing} from "../../../../models/interfaces/interfaces";
import {take, takeUntil} from "rxjs";
import {ConfirmationService, MessageService} from "primeng/api";

@Component({
    selector: 'app-create-new-product',
    templateUrl: './create-new-product.component.html',
    styleUrls: ['./create-new-product.component.scss']
})
export class CreateNewProductComponent implements OnInit, OnDestroy {
    public typesOfClothing: ITypeOfClothing[];
    public genders: string[];
    public sizes: ISizes[];
    public newMerchForm: FormGroup;
    public clearEvent = new EventEmitter<void>();
    private destroySub = new EventEmitter<void>();

    constructor(private adminRestService: AdminRestService, private messageService: MessageService, private confirmationService: ConfirmationService) {
    }

    ngOnInit(): void {
        this.initClothingTypes();
        this.initSizes();
        this.genders = ['Male', 'Female'];
        this.newMerchForm = new FormGroup({
            type: new FormControl(null, [Validators.required]),
            title: new FormControl('', [Validators.required]),
            brand: new FormControl(''),
            imgSrc: new FormControl([]),
            description: new FormControl(''),
            price: new FormControl(''),
            gender: new FormControl(''),
            size: new FormControl(null, [Validators.required]),
            isSale: new FormControl(false),
            discount: new FormControl(0),
        })
        this.adminRestService.sizeChangeEvent
            .pipe(takeUntil(this.destroySub))
            .subscribe(() => {
                this.initSizes();
            })
        this.adminRestService.clothingTypeChangeEvent
            .pipe(takeUntil(this.destroySub))
            .subscribe(() => {
                this.initClothingTypes();
            })
    }

    initClothingTypes() {
        this.adminRestService.getAllClothingTypes()
            .pipe(take(1))
            .subscribe((types:  ITypeOfClothing[]) => {
                this.typesOfClothing = types;
            })
    }

    initSizes() {
        this.adminRestService.getAllSizes()
            .pipe(take(1))
            .subscribe((sizes: ISizes[]) => {
                this.sizes = sizes;
            })
    }

    imgChanged(files: any[]) {
        this.newMerchForm.controls['imgSrc'].setValue(files);
    }

    submit(): void {
        const newItem = this.newMerchForm.getRawValue();
        this.adminRestService.sendNewMerch(newItem)
            .subscribe({
                next: (response: IProduct) => {
                    this.messageService.add({
                        severity: 'success',
                        summary: `${response.title} Успешно создан`,
                        life: 1500
                    });
                    this.newMerchForm.reset();
                    this.clearEvent.next();
                },
                error: (err: HttpErrorResponse) => {
                    this.messageService.add({severity: 'error', summary: err.message});
                }
            })
    }

    ngOnDestroy() {
        this.destroySub.next();
        this.destroySub.complete();
    }

}
