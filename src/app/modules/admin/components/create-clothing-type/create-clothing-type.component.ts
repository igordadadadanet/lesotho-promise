import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {HttpErrorResponse} from "@angular/common/http";
import {AdminRestService} from "../../services/admin-rest.service";
import {ISizes, ITypeOfClothing} from "../../../../models/interfaces/interfaces";
import {take} from "rxjs";
import {ConfirmationService, MessageService} from "primeng/api";


@Component({
    selector: 'app-create-clothing-type',
    templateUrl: './create-clothing-type.component.html',
    styleUrls: ['./create-clothing-type.component.scss']
})
export class CreateClothingTypeComponent implements OnInit {
    public newTypeOfClothingForm: FormGroup;
    public allTypes: ITypeOfClothing[];

    constructor(private adminRestService: AdminRestService, private messageService: MessageService, private confirmationService: ConfirmationService) {
    }

    ngOnInit(): void {
        this.newTypeOfClothingForm = new FormGroup({
            name: new FormControl('', [Validators.required]),
        });
        this.getTypes();
    }

    private getTypes() {
        this.adminRestService.getAllClothingTypes()
            .pipe(take(1))
            .subscribe((types: ITypeOfClothing[]) => {
                this.allTypes = types;
            })
    }

    submit(): void {
        const newType = this.newTypeOfClothingForm.getRawValue();
        this.adminRestService.sendNewTypeOfClothing(newType)
            .subscribe({
                next: (response: ITypeOfClothing) => {
                    this.messageService.add({
                        severity: 'success',
                        summary: `${response.name} Успешно создан`,
                        life: 1500
                    });
                    this.newTypeOfClothingForm.reset();
                    this.getTypes();
                    this.adminRestService.clothingTypeChangeEvent.next();
                },
                error: (err: HttpErrorResponse) => {
                    this.messageService.add({severity: 'error', summary: err.message});
                }
            })
    }

    public confirmDelete(id: string): void {
        this.confirmationService.confirm({
            message: 'Do you want to delete this record?',
            header: 'Delete Confirmation',
            icon: 'pi pi-info-circle',
            accept: () => {
                this.adminRestService.deleteType(id)
                    .pipe(take(1))
                    .subscribe(response => {
                        this.adminRestService.clothingTypeChangeEvent.next();
                        this.messageService.add({severity: 'success', summary: `Запись успешно удалена`, life: 1500});
                        this.getTypes();
                    })
            },
            reject: () => {
            }
        });
    }

}
