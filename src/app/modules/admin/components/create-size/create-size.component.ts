import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AdminRestService} from "../../services/admin-rest.service";
import {HttpErrorResponse} from "@angular/common/http";
import {ConfirmationService, MessageService} from "primeng/api";
import {ISizes} from "../../../../models/interfaces/interfaces";
import {take} from "rxjs";
import {Size} from "../../../../models/constructors/size";

@Component({
    selector: 'app-create-size',
    templateUrl: './create-size.component.html',
    styleUrls: ['./create-size.component.scss']
})
export class CreateSizeComponent implements OnInit {
    public sizeForm: FormGroup;
    public allSizes: ISizes[];
    private canEditIds: {[key: string]: boolean} = {};

    constructor(private adminRestService: AdminRestService, private messageService: MessageService, private confirmationService: ConfirmationService) {
    }

    ngOnInit(): void {
        this.sizeForm = new FormGroup({
            name: new FormControl('', [Validators.required]),
        });
        this.getAllSizes();
    }

    private getAllSizes(): void {
        this.adminRestService.getAllSizes()
            .pipe(take(1))
            .subscribe((sizes: ISizes[]) => {
                this.allSizes = sizes;
            });
    }

    public submit(): void {
        const newType = this.sizeForm.getRawValue();
        this.adminRestService.sendNewSize(newType)
            .subscribe({
                next: (response: ISizes) => {
                    this.adminRestService.sizeChangeEvent.next();
                    this.messageService.add({
                        severity: 'success',
                        summary: `${response.name} Успешно создан`,
                        life: 1500
                    });
                    this.sizeForm.reset();
                    this.getAllSizes()
                },
                error: (err: HttpErrorResponse) => {
                    this.messageService.add({severity: 'error', summary: err.message});
                }
            })
    }

    public confirmDelete(id: string): void {
        this.confirmationService.confirm({
            message: 'Do you want to delete this record?',
            header: 'Delete Confirmation',
            icon: 'pi pi-info-circle',
            accept: () => {
                this.adminRestService.deleteSize(id)
                    .pipe(take(1))
                    .subscribe(response => {
                        this.adminRestService.sizeChangeEvent.next();
                        this.messageService.add({severity: 'success', summary: `Запись успешно удалена`, life: 1500});
                        this.getAllSizes();
                    })
            },
            reject: () => {
            }
        });
    }

    public isCanEdit(id: string): boolean {
        return this.canEditIds[id];
    }

    public edit(id: string, isEdit: boolean): void {
        if (!isEdit) {
            this.getAllSizes();
        }
        this.canEditIds[id] = isEdit;
    }

    public confirmUpdate(id: string, newValue: string): void {
        const newSize = new Size(id, newValue);
        this.adminRestService.editSize(newSize)
            .pipe(take(1))
            .subscribe(() => {
                this.adminRestService.sizeChangeEvent.next();
                this.messageService.add({severity: 'success', summary: `Запись успешно изменена`, life: 1500});
                this.getAllSizes();
                this.edit(newSize._id, false);
            })
    }

}
