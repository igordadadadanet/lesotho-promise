import {Component, OnInit} from '@angular/core';
import {IProduct} from "../../../../models/interfaces/interfaces";
import {Subscription} from "rxjs";


@Component({
    selector: 'app-admin-component',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
    private routeSubscription!: Subscription;
    public products: IProduct[] = [];
    public loading = false;

    constructor() {
    }

    ngOnInit(): void {
        // this.routeSubscription = this.queryParamsService.activatedRoute.queryParams
        //     .subscribe(() => {
        //
        //       this.loading = true;
        //       this.state.getData(this._url, true)
        //           .subscribe((data: DataType[]) => {
        //
        //             if (isType<IProduct>(data, 'type')) {
        //
        //               this.loading = false;
        //               this.products = data;
        //               // this.assortmentArray = this.paginationService.rendererPageData(data);
        //               //каждый раз при рендере продукт листа
        //             }
        //           });
        //     });
    }

}
