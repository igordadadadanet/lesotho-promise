import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {takeUntil} from "rxjs";


@Component({
    selector: 'app-img-downloader',
    templateUrl: './img-downloader.component.html',
    styleUrls: ['./img-downloader.component.scss']
})
export class ImgDownloaderComponent implements OnInit, OnDestroy {
    public images: string[] = [];
    public imgFiles: File[] = [];
    @Output() fileChanged = new EventEmitter<any[]>();
    @Input() clearDescription: EventEmitter<void>;
    private destroySub = new EventEmitter<void>();
    public imgForm: FormGroup;

    ngOnInit() {
        this.imgForm = new FormGroup({
            file: new FormControl('', [Validators.required]),
            fileSource: new FormControl('', [Validators.required])
        });
        this.clearDescription
            .pipe(takeUntil(this.destroySub))
            .subscribe(() => {
                this.imgForm.reset();
                this.images = [];
                this.imgFiles = [];
            })
    }

    onFileChange(event: any) {
        if (event.target.files && event.target.files[0]) {
            const filesAmount = event.target.files.length;
            for (let i = 0; i < filesAmount; i++) {
                this.imgFiles.push(event.target.files[i]);
                const reader = new FileReader();
                reader.onload = (event: any) => {
                    this.images.push(event.target.result);
                    this.imgForm.patchValue({
                        fileSource: this.images
                    });
                }
                reader.readAsDataURL(event.target.files[i]);
            }
            this.fileChanged.next(this.images);
        }
    }

    ngOnDestroy() {
        this.destroySub.next();
        this.destroySub.complete();
    }

}
