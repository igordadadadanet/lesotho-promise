import {
    trigger,
    animate,
    transition,
    style,
    query,
    stagger
} from '@angular/animations';


export const formCheckAnimations = trigger('formCheckAnimations', [
    transition("* <=> *", [
        query(':enter',
            [style({ opacity: 0, transform: "translateX(-100%)" }), stagger('60ms', animate('200ms ease-out', style({ opacity: 1, transform: "translateX(0)" })))],
            { optional: true }
        )
    ])
]);