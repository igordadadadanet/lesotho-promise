import {
    trigger,
    animate,
    style,
    query as q,
    transition,
    AnimationMetadata,
    state
} from "@angular/animations";


const timing = 250;

const query = (s: string, a: AnimationMetadata | AnimationMetadata[], o = {optional: true}) => q(s, a, o);

export const wrapperTransition = trigger("wrapperTransition", [
    state("start", style({height: "*"})),
    state("end", style({height: "0px", opacity: 0})),
    transition("end => start", [
        animate(timing)
    ]),
    transition("start => end", [
        style({height: "!"}),
        animate(timing, style({height: "0px", opacity: 0})),
        query(":leave", animate(timing))
    ])
])