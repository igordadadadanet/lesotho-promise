//тип формата JackNJones конвертируются через enum там где это необходимо
export enum BrandTitle {
    'NIKE' = 'NIKE',
    'BenSherman' = 'Ben Sherman',
    'RalphLauren' = 'Ralph Lauren',
    'FredPerry' = 'Fred Perry',
    'Everlast' = 'Everlast',
    'HugoBoss' = 'Hugo Boss',
    'JackNJones' = 'Jack & Jones',
    'Levis' = 'Levis',
    'Selected' = 'Selected',
    'TheNorthFace' = 'The North Face',
    'Gap' = 'Gap'
}

export enum TypeTitle {
    'jeans' = 'Джинсы',
    'outerwear' = 'Верхняя одежда',
    'shirt' = 'Рубашки',
    'sweater' = 'Свитера',
    'sweatshirt' = 'Свитшоты',
    't-shirt' = 'Футболки',
    'hoodie' = 'Худи',
}

export enum GenderEnum {
    'male' = 'Мужская одежда',
    'female' = 'Женская одежда'
}