import { BrandTitle, TypeTitle } from "./enums";

//typesOfDataInterfaces////
export type BrandList =
    'NIKE'
    | 'BenSherman'
    | 'RalphLauren'
    | 'FredPerry'
    | 'Everlast'
    | 'HugoBoss'
    | 'JackNJones'
    | 'Levis'
    | 'Selected'
    | 'TheNorthFace';

export interface ITypeOfClothing {
    _id: string,
    name: string
}
//dataInterfaces/////
export interface MainGoodsCard {
    title: string,
    imgSrc: string,
    link: string,
    type: string
}

export interface Brand {
    title: BrandList,
    imgSrc: string,
    isLarge?: boolean,
    logoSrc: string,
    link: string
}

export interface Contacts {
    phone: string,
    location: {
        position: string,
        address: string,
        iframePosition: string
    },
    email: string
}

export interface Socials {
    name: string,
    url: string,
    icon: string
}

export interface IProduct {
    _id: string,
    type: ITypeOfClothing,
    title: string,
    brand: BrandList,
    imgSrc: string[],
    description: string,
    price: number,
    gender: "male" | "female",
    size: ISizes,
    isSale?: boolean,
    discount?: number,


}

export interface ISizes {
    _id: string,
    name: string
}

export type GendersType = 'male' | 'female';


export interface CarouselItem {
    title: string,
    description: string,
    imgSrc: string
}


export type DataType = IProduct | MainGoodsCard | Brand | Contacts | Socials | CarouselItem;

export function isType<T extends DataType>(data: DataType[], key: keyof T): data is T[] {
    return (data as T[])[0][key] !== undefined;
}

/////////

export interface FilterParams {
    type?: ITypeOfClothing | ITypeOfClothing[],
    title?: string | string[],
    brand?: BrandList | BrandList[],
    gender?: GendersType,
    size?: ISizes
}

type EnumToType<T> = {
    [P in keyof T]: T[P]
}

export type TBrandTitle = EnumToType<typeof BrandTitle>;

export type TTypeTitle = EnumToType<typeof TypeTitle>;

export type RequestState = 'loading' | 'correct' | 'failure';

export interface IMongoResultingMessage {
    acknowledged : boolean,
    deletedCount : number
}
