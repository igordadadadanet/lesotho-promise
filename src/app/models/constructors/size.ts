import {ISizes} from "../interfaces/interfaces";

export class Size implements ISizes {
    _id: string;
    name: string;
    constructor(id: string, name: string) {
        this._id = id;
        this.name = name;
    }
}