import { Component, OnDestroy, OnInit } from '@angular/core';
import { Brand, DataType, isType } from "../../models/interfaces/interfaces";
import { StateService } from "../../services/state.service";
import { Subscription } from "rxjs";


@Component({
    selector: 'app-brand-list',
    templateUrl: './brand-list.component.html',
    styleUrls: ['./brand-list.component.scss']
})
export class BrandListComponent implements OnInit, OnDestroy {
    private _url = 'assets/jsons/brands.json';
    private brandsSubscription!: Subscription;

    public brands: Brand[] = [];

    constructor(private state: StateService) {
    }

    ngOnInit(): void {
        this.brandsSubscription = this.state.getData(this._url)
            .subscribe((data: DataType[]) => {
                if (isType<Brand>(data, 'title')) this.brands = data;
            })
    }

    ngOnDestroy() {
        this.brandsSubscription.unsubscribe();
    }

}
