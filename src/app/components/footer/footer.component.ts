import { Component, OnInit } from '@angular/core';
import { Contacts, DataType, isType, Socials } from "../../models/interfaces/interfaces";
import { StateService } from "../../services/state.service";


@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
    private _socialUrl = 'assets/jsons/social.json';
    private _contactUrl = 'assets/jsons/contacts.json';

    public socialMedia: Socials[] = [];

    public contacts: Contacts = {
        phone: 'loading...',
        email: 'loading...',
        location: {
            position: 'loading...',
            address: 'loading...',
            iframePosition: ''
        }
    }

    constructor(private state: StateService) {
    }

    ngOnInit(): void {
        this.state.getData(this._contactUrl)
            .subscribe((contacts: DataType[]) => {
                if (isType<Contacts>(contacts, 'phone')) this.contacts = contacts[0]
            })
        this.state.getData(this._socialUrl)
            .subscribe((social: DataType[]) => {
                if (isType<Socials>(social, 'url')) this.socialMedia = social;
            })
    }

}
