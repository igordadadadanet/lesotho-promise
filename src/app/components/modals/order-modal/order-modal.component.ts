import {Component, Input, OnInit} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap/modal";


@Component({
    selector: 'order-modal',
    templateUrl: './order-modal.component.html',
    styleUrls: ['./order-modal.component.scss']
})
export class OrderModalComponent implements OnInit {
    @Input('status') status: string = 'loading';

    constructor(public bSModalRef: BsModalRef) {
    }

    ngOnInit(): void {
    }

}
