import {AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {StateService} from "../../services/state.service";
import {WindowNavigateService} from "../../services/window-navigate.service";
import {AuthService} from "../../modules/auth/services/auth.service";


@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, AfterViewInit {
    @Input() typesOfClothing: any[] = [];


    constructor(private state: StateService, private windowNavService: WindowNavigateService, public authService: AuthService) {
    }

    @ViewChild('merchandiseWindow') merchandiseWindow!: ElementRef;

    ngOnInit(): void {
    }

    ngAfterViewInit() {
        this.getHeaderHeight();
    }

    // получаем высоту header
    getHeaderHeight() {
        this.windowNavService.getHeight(this.merchandiseWindow.nativeElement.getBoundingClientRect().height - 80);
    }

}
