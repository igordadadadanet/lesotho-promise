import {Component, OnInit} from '@angular/core';
import {CarouselItem, DataType, isType} from "../../models/interfaces/interfaces";
import {StateService} from "../../services/state.service";
import {take} from "rxjs";


@Component({
    selector: 'app-carousel',
    templateUrl: './carousel.component.html',
    styleUrls: ['./carousel.component.scss'],
})
export class CarouselComponent implements OnInit {
    private _url = 'assets/jsons/carouselItems.json';
    public carouselArray: CarouselItem[] = [];

    constructor(private state: StateService) {
    }

    ngOnInit(): void {
        this.state.getData(this._url)
            .pipe(take(1))
            .subscribe((data: DataType[]) => {
                if (isType<CarouselItem>(data, 'imgSrc')) this.carouselArray = data;
            })
    }

}
