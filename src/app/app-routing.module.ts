import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MainComponent} from "./modules/main/components/main/main.component";


const routes: Routes = [
    {path: '', component: MainComponent},
    {path: 'assortment', loadChildren: () => import('./modules/assortment/assortment.module').then(m => m.AssortmentModule)},
    {path: 'order', loadChildren: () => import('./modules/order/order.module').then(m => m.OrderModule)},
    {path: 'contacts', loadChildren: () => import('./modules/order/order.module').then(m => m.OrderModule)},
    {path: 'return', loadChildren: () => import('./modules/refund/refund.module').then(m => m.RefundModule)},
    {path: 'about', loadChildren: () => import('./modules/about/about.module').then(m => m.AboutModule)},
    {path: 'auth', loadChildren: () => import('./modules/auth/auth.module').then(m => m.AuthModule)},
    {path: 'admin-panel', loadChildren: () => import('./modules/admin/admin.module').then(m => m.AdminModule)},
    {path: '**', component: MainComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
