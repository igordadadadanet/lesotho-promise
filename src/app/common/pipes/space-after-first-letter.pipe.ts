import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
    name: 'spaceAfterFirstLetter'
})
export class SpaceAfterFirstLetterPipe implements PipeTransform {
    transform(value: string): string {
        return `${value[0]} ${value.substring(1)}`;
    }

}
