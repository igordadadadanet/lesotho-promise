import { Pipe, PipeTransform } from '@angular/core';
import { GendersType } from "../../models/interfaces/interfaces";
import { GenderEnum } from "../../models/interfaces/enums";


@Pipe({
  name: 'genderTitle'
})
export class GenderTitlePipe implements PipeTransform {

  private genderTitle = GenderEnum

  transform(value: GendersType): string {
    return this.genderTitle[value];
  }

}
