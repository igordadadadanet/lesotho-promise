import { Pipe, PipeTransform } from '@angular/core';
import { TypeTitle } from "../../models/interfaces/enums";
import { TTypeTitle } from "../../models/interfaces/interfaces";


@Pipe({
    name: 'typeTitle'
})
export class TypeTitlePipe implements PipeTransform {
    private typeTitle: TTypeTitle = TypeTitle

    transform(value: keyof TTypeTitle): string {
        return this.typeTitle[value];
    }

}
