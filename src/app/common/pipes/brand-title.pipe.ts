import { Pipe, PipeTransform } from '@angular/core';
import { BrandTitle } from "../../models/interfaces/enums";
import { TBrandTitle } from "../../models/interfaces/interfaces";


@Pipe({
    name: 'brandTitle'
})
export class BrandTitlePipe implements PipeTransform {
    private brandTitle: TBrandTitle = BrandTitle;

    transform(value: keyof TBrandTitle): string {
        return this.brandTitle[value]
    }

}