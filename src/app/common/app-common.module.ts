import {NgModule} from '@angular/core';
import {SpinnerComponent} from "./spinner/spinner.component";
import {SafePipe} from "./pipes/safe.pipe";
import {SpaceAfterFirstLetterPipe} from './pipes/space-after-first-letter.pipe';
import {BrandTitlePipe} from './pipes/brand-title.pipe';
import {TypeTitlePipe} from './pipes/type-title.pipe';
import {GenderTitlePipe} from './pipes/gender-title.pipe';


@NgModule({
    declarations: [
        SpinnerComponent,
        SafePipe,
        SpaceAfterFirstLetterPipe,
        BrandTitlePipe,
        TypeTitlePipe,
        GenderTitlePipe,
    ],
    exports: [
        SpinnerComponent,
        SafePipe,
        SpaceAfterFirstLetterPipe,
        BrandTitlePipe,
        TypeTitlePipe,
        GenderTitlePipe
    ]
})
export class AppCommonModule {
}