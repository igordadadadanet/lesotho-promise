import {InjectionToken, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from "@angular/common/http";
import {AppRoutingModule} from './app-routing.module';
import {AppCommonModule} from "./common/app-common.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {AppComponent} from './app.component';
import {HeaderComponent} from "./components/header/header.component";
import {CarouselComponent} from './components/carousel/carousel.component';
import {BrandListComponent} from './components/brand-list/brand-list.component';
import {FooterComponent} from './components/footer/footer.component';
import {BackgroundDirective} from './directives/background.directive';
import {StateService} from "./services/state.service";
import {CacheService} from "./services/cache.service";
import {AuxiliaryService} from "./services/auxiliary.service";
import {WindowNavigateService} from "./services/window-navigate.service";
import {TooltipModule} from "ngx-bootstrap/tooltip";
import {BsModalService} from "ngx-bootstrap/modal";
import {MainComponent} from "./modules/main/components/main/main.component";
import {ProductsComponent} from "./modules/main/components/products/products.component";
import {ProductItemComponent} from "./modules/main/components/product-item/product-item.component";
import {OrderModalComponent} from "./components/modals/order-modal/order-modal.component";
import {ConfirmationService, MessageService} from "primeng/api";
import {ToastModule} from "primeng/toast";
import {ConfirmDialogModule} from "primeng/confirmdialog";
import {AdminRestService} from "./modules/admin/services/admin-rest.service";


export const SERVER_URL = new InjectionToken<string>('SERVER_URL');

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        MainComponent,
        ProductsComponent,
        ProductItemComponent,
        CarouselComponent,
        BrandListComponent,
        BackgroundDirective,
        FooterComponent,
        OrderModalComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        AppCommonModule,
        BrowserAnimationsModule,
        TooltipModule.forRoot(),
        ToastModule,
        ConfirmDialogModule,
    ],
    providers: [
        StateService,
        CacheService,
        AuxiliaryService,
        WindowNavigateService,
        BsModalService,
        {provide: SERVER_URL, useValue: 'http://localhost:3000'},
        MessageService,
        ConfirmationService,
        AdminRestService
    ],
    exports: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
