import {
    Directive,
    ElementRef,
    HostListener,
    Input,
    Renderer2
} from '@angular/core';


@Directive({
    selector: '[appBackground]'
})
export class BackgroundDirective {
    constructor(private element: ElementRef, private renderer: Renderer2) {
    }

    @Input() hoverBackground: string = 'none';

    @HostListener('mouseenter') onMouseEnter() {
        this.renderer.setStyle(this.element.nativeElement, 'background-image', `url(${this.hoverBackground})`);
        this.renderer.setStyle(this.element.nativeElement, 'background-attachment', `inherit`);
    }

    @HostListener('mouseleave') onMouseLeave() {
        this.renderer.removeStyle(this.element.nativeElement, 'background-image');
        this.renderer.setStyle(this.element.nativeElement, 'background-attachment', `fixed`);
    }

}