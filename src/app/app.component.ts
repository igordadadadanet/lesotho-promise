import {Component, OnInit} from '@angular/core';
import {fadeAnimation} from "./animations/fade.animations";
import {StateService} from "./services/state.service";


@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styles: [],
    animations: [fadeAnimation]
})
export class AppComponent implements OnInit {
    public title = 'lesotho-promise';

    constructor(public stateService: StateService) {
    }

    ngOnInit() {
        this.stateService.initSettings();
    }
}