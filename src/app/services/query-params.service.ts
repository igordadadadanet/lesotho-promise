import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { FilterParams } from "../models/interfaces/interfaces";
import { AuxiliaryService } from "./auxiliary.service";


@Injectable()
export class QueryParamsService {
    public queryParams!: any;

    constructor(public router: Router, public activatedRoute: ActivatedRoute, private auxiliary: AuxiliaryService) {
        this.activatedRoute.queryParams.subscribe((params: FilterParams) => {
            this.queryParams = this.auxiliary.keyOfObjToArray(params);
        })
    }

    addQueryParams(typeOfParam: string, param: string | number) {
        let setQueryParams;

        if(typeOfParam === 'page' || typeOfParam === 'itemsPerPage') {
            setQueryParams = {
                [typeOfParam]: param,
                'youCanRemoveMultiple': null,
            }
        } else {
            setQueryParams = {
                [typeOfParam]: this.queryParams[typeOfParam] ? [...this.queryParams[typeOfParam], param] : param,
                'youCanRemoveMultiple': null,
            }
        }

        this.router.navigate(
            [],
            {
                queryParams: setQueryParams,
                queryParamsHandling: 'merge'
            }
        )
    }

    deleteQueryParams(typeOfParam: string, param: string) {
        this.router.navigate(
            [],
            {
                queryParams: {
                    [typeOfParam]: this.queryParams[typeOfParam] && this.queryParams[typeOfParam][1] ? this.queryParams[typeOfParam].filter((item: string) => item !== param) : null,
                    'youCanRemoveMultiple': null,
                },
                queryParamsHandling: 'merge'
            }
        )
    }

}