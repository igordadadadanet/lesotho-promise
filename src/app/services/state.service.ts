import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { ActivatedRoute, Params } from "@angular/router";
import {Observable, of, take, tap} from "rxjs";
import { map } from "rxjs/operators";

import { CacheService } from "./cache.service";
import {DataType, FilterParams, isType, IProduct, ITypeOfClothing} from "../models/interfaces/interfaces";
import { AuxiliaryService } from "./auxiliary.service";
import {AdminRestService} from "../modules/admin/services/admin-rest.service";


@Injectable()
export class StateService {
    constructor(private cache: CacheService, private http: HttpClient, public auxiliary: AuxiliaryService, private route: ActivatedRoute, private adminRestService: AdminRestService) {
    }

    public filteredProductArray: IProduct[] = [];
    public typesOfClothing: ITypeOfClothing[];

    //если в кэше нет этих данных, делаем get запрос, после чего записываем эти данные в кэш
    public getData(url: string, filter: boolean = false): Observable<DataType[]> {
        //переменная содержит следующте квери параметры = {...}
        const queryParams = this.route.snapshot.queryParams;
        //переменная содержат параметры true or false
        const haveParams = Boolean(Object.keys(queryParams)[0]);
        //если в кэше есть обьект по данному ключу, то массив cachedResult содержит обьекты из кэша
        if (this.cache.getCacheData(url)) {
            const cachedResult = this.cache.getCacheData(url);
            if(isType<IProduct>(cachedResult, 'type')) this.filteredProductArray = cachedResult;
            //если есть параметры квери и getData настроен на фильтрацию то возвращаем массив отфильтрованный по этим показателем, иначе возвращаем полный массив данных из кэша
            return filter && haveParams ? of(this.dataFilter(cachedResult, queryParams)) : of(cachedResult)
        } else {
            //если в кэше нет данных по этому ключу, то делаем get запрос
            return this.http.get<DataType[]>(url)
                .pipe(
                    map((items: DataType[]) => {
                        //записываем в кэш
                        this.cache.setCacheData(url, items);
                        if(isType<IProduct>(items, 'type')) this.filteredProductArray = items;
                        //если есть настройка на фильтр, есть квери параметры и это данные с продуктом возвращаем эти данные, которые прошли фильтрацию. иначе просто все данные из кэша по ключу
                        return filter && haveParams && isType<IProduct>(items, 'type') ? this.dataFilter(items, queryParams) : items;
                    })
                )
        }
    }

    public getFilterParams(type: 'brand' | 'type' | 'gender' | 'size') {

        //все параметры строки
        const queryParams = this.route.snapshot.queryParams;
        //ищем первое сова=падение параметров по data
        const mainFilterType = Object.keys(queryParams).find((queryValue) => {
            return queryValue === 'type' || queryValue === 'brand' || queryValue === 'gender' || queryValue === 'size'
        });

        return this.getData('http://localhost:5000/products', mainFilterType !== type)
            .pipe(
                map((arr: DataType[]) => {

                    if (isType<IProduct>(arr, 'size')) {
                        console.log(arr)
                        return arr.reduce((accumulator: any, value: any) => {

                            return {...accumulator, [value[type]]: (accumulator[value[type]] || 0) + 1};
                        }, {});
                    }
                    return arr
                })
            )
    }

    // доработать: ts исключения, повторение кода
    //фильтр
    public dataFilter(data: IProduct[], params: Params) {
        //фильтры для карточек продукта
        const dataFilterParamsValue: (keyof FilterParams)[] = ['type', 'brand', 'gender', 'size'];
        //те фильтры, которые есть в параметрах строки и подходят под фильтры карточек продукта
        const dataFilterParams: (keyof FilterParams)[] = (Object.keys(params) as (keyof FilterParams)[]).filter(item => dataFilterParamsValue.includes(item) && item);

        //если таких нет - вернуть просто ata
        if (!dataFilterParams[0]) return this.filteredProductArray;

        // переменная для приоритетного типа фильтра.
        const primaryFilterType: keyof FilterParams = dataFilterParams[0];

        dataFilterParams.forEach((type: keyof FilterParams) => {
            if (primaryFilterType !== type) {
                // @ts-ignore
                this.auxiliary.toArray(params[type]).forEach((value: string) => {

                    this.filteredProductArray = this.filteredProductArray.filter((product: IProduct) => {
                        return product[type] === value
                    })
                })
            } else {
                this.filteredProductArray = [];
                // @ts-ignore
                this.auxiliary.toArray(params[type]).forEach((value: string) => {
                    this.filteredProductArray.push(...data.filter((product: IProduct) => {
                        return product[type] === value && this.auxiliary.isValueInArray<IProduct>(this.filteredProductArray, product, type);
                    }))
                })
            }
        })
        return this.filteredProductArray;
    }

    public initSettings() {
        this.adminRestService.getAllClothingTypes()
            .pipe(take(1))
            .subscribe((types: ITypeOfClothing[]) => {
                this.typesOfClothing = types;
            })
    }

}