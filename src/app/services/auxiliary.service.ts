import { Injectable } from '@angular/core';


@Injectable()
export class AuxiliaryService {
    //метод- такой обьект есть в массиве
    isValueInArray<T>(arrayWithData: T[], object: T, key: keyof T): boolean {
        return !Boolean(arrayWithData.filter((obj: T) => {
            return obj[key] === object[key]
        })[0])
    }

    //если строка, то массив строк
    public toArray(value: string | string[]): string[] {
        return Array.isArray(value) ? value : [value];
    }

    //значение к ключу обьекта из строки в массив строк
    public keyOfObjToArray(object: any) {
        const keys = Object.keys(object);
        for (let key of keys) {
            object = {...object, [key]: this.toArray(object[key])}
        }
        return object;
    }

    //форматируем входящий обьект с названиями брендов/ типов и их колличеством в массив обьектов
    prepareAsideParams(data: any) {
        const keys = Object.keys(data);
        const result = []
        for (let key of keys) {
            result.push({title: key, count: data[key]})
        }
        return result;
    }

}