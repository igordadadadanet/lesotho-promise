import { Injectable } from '@angular/core';


@Injectable()
export class CacheService {
  private data = new Map();

  getCacheData(key: string) {
    return this.data.get(key);
  }

  setCacheData(key : string, value: any) {
    this.data.set(key, value);
  }

}
