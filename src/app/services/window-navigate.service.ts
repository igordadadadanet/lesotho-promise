import { Injectable } from '@angular/core';


@Injectable()
export class WindowNavigateService {
  private height: number = 0;

  scrollTo() {
    window.scrollTo({
      top: this.height
    })
  }

  getHeight(height: number): void {
    this.height = height;
  }

}
