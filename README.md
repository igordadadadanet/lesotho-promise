# LesothoPromise

Онлайн магазин одежды написанный на фреймворке Angular (version 13.0.1.);

# Описание

Angular Routing, Query Parameters, Services, Pipes, Directives, Angular animations, RxJs, Получение данных из JSON (в будущем возможно реализовать запросы к backend). Фильтр товаров, поиск по товарам.

### To-Do:

+ ~~Доделать фильтр по товарам;~~
+ ~~Анимация router-outlet merchandise;~~
+ Поиск товаров `(необязательно)`;
+ Рефактор, оптимизация;
+ Пагинатор;
+ Переделать header в мобильном разрешении;
+ ~~При открытии товара scroll to header;~~
+ ~~Скачет aside список;~~
+ переделать получение данных (подписка в подписке)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
